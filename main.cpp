#include "list.h"
using namespace std;

// The purpose of this main.cpp file is to call the functions in list.h and use
// varibles userDay (to get a certain dau from the user), userActivity ( to get a
// certain activity from the user, allocating the userEvent so that the user can 
// input all the infomation needed for a event, create an object to call the functions 
// in the DayLLL class, result (used as a wayt to test if the function worked for not)
// common to use if(result == 0) as a way to test/output error message, and choice is
// used to prompt user ti run differnet functions from the menu.

int main()
{
	char userDay[SIZE];	// static array to get day input from user
	char userActivity[SIZE];	// static array to get activity input from user to find what activity
	char order;	// char to get the user choice of what order they want the activity to gi
	event userEvent;	// need to allocate userEvent memory since dynamic allocated
	userEvent.time = new char[SIZE];	
	userEvent.activity = new char[SIZE];
	userEvent.location = new char[SIZE];
	userEvent.budget =0.0;
	dayLLL myDay;	// class object
	int result {0};	// error checker if 0 there is error
	int choice {0};	// user menu choice

	cout << endl << setfill('-') << setw(109) << endl
	     << "\nWelcome to the One Week Trip Planner!\n" << endl
	     << "This program will keep track of you plans for each day of the week." << endl
	     << "Note that there cannot be any duplicate days. For a day you are able" << endl
	     << "to enter in what you have planned: Time, Activity, Location, Budget." << endl
	     << "Please enter time in plans and time in chronological order; however," << endl
	     << "you also have the option of append to the end or the beginning."
	     << setfill('-') << setw(75) << endl << endl;

	do
	{
		//Print put menu for user to select choice
		cout << "\n\nMENU" << endl
		     << "[1] Add a day" << endl
		     << "[2] Add an activity to a day" << endl
		     << "[3] Delete day" << endl
		     << "[4] Find an activity" << endl
		     << "[5] Display a Day" << endl
		     << "[6] Display All" << endl
		     << "[0] Exit" << endl;

		cout << "Enter your choice: ";
		cin >> choice;
 		while(!cin || cin.peek() != '\n')       // validate the user enters number not a char
                          {
                                  cin.clear();
                                  cin.ignore(100, '\n');
                                  cout << "Invalid Number. Try again." << endl
                                       << "Enter your choice: ";
                                  cin >> choice;
                          }
                          cin.get();

		if(choice == 1) 	// choice is 1 then run addDay function;
		{
			cout << "\nEnter a Day: ";
			cin.get(userDay, SIZE, '\n');
			cin.ignore(100, '\n');

			result = myDay.addDay(userDay);
			if(result == 0)	
			{
				cout << "\nThat day already been entered" << endl;
			}
			else if(result == 1)
			{
				cout << "\nDay has been successfully added!" << endl;
			}
		}
		if(choice == 2)		// choice is 2 then add an Activity
		{
			cout << "\nEnter day you would like to add an activity to: ";
			cin.get(userDay, SIZE, '\n');
                        cin.ignore(100, '\n');
			cout << "\nEnter the time of the activity: ";
                        cin.get(userEvent.time, SIZE, '\n');
                        cin.ignore(100, '\n');
                        cout << "Enter the activity: ";
                        cin.get(userEvent.activity, SIZE, '\n');
                        cin.ignore(100, '\n');
                        cout << "Enter the location of the activity: ";
                        cin.get(userEvent.location, SIZE, '\n');
                        cin.ignore(100, '\n');
                        cout << "Enter the budget for your activity: ";
                        cin >> userEvent.budget;
			while(!cin || cin.peek() != '\n')	// validate the user enters number not a char
        		{
                		cin.clear();
                		cin.ignore(100, '\n');
               	 		cout << "Invalid Number. Try again." << endl
                     		     << "Enter the budget for your activity: ";
				cin >> userEvent.budget;
			}
			cin.get();

			cout << "\nWould you like to add this activity at the beginning"
			     << " or at the end of your list of plans" << endl
			     << "Enter 'Y' to add in the front or Enter any key to add to the end: ";
			cin >> order;
			cin.ignore(100, '\n');
                        result = myDay.addActivity(userDay, order, userEvent);
			if(result == 0)	
				cout << "\nFailed to add activity." << endl;
			if(result == 1)
				cout << "\nActivity has been successfully added!" << endl;
		}
		if(choice == 3)		// choice 3 then delete a day
		{
			cout << "\nEnter day you would like to delete: ";
                        cin.get(userDay, SIZE, '\n');
                        cin.ignore(100, '\n');
			result = myDay.deleteDay(userDay); 
			if(result == 0)
				cout << "\nFailed to delete Day" << endl;
			if(result == 1)
				cout << "\nDay was successfully deleted!" << endl;
		}
		if(choice == 4)		// displays the day with that plan	
		{
			cout << "\nEnter an Activity you would like to find: ";
                        cin.get(userActivity, SIZE, '\n');
                        cin.ignore(100, '\n');
			result = myDay.findActivity(userActivity); 
			if(result == 0)
				cout << "\nActivity not found" << endl;
			if(result == 1)
				cout << "\nActivity found" << endl;
		}
		if(choice == 5)		// displays the day and the plans of that day	
		{
			cout << "\nEnter a day you would like to display: ";
                        cin.get(userDay, SIZE, '\n');
                        cin.ignore(100, '\n');
			result = myDay.displayDay(userDay);
			if(result == 0)
                                cout << "\nThere is no day with that name" << endl;
		}
		if(choice == 6)		// displays all plans and days
		{
			result = myDay.displayAll();
			if(result == 0)
				cout << "No days/plans have been entered yet" << endl;
		}




	}while(choice != 0);
	cout << "Thank You !" << endl;
	// since we used new in main we have to deallocate the memory
	delete [] userEvent.time;
	delete [] userEvent.activity;
	delete [] userEvent.location;

	return 0;
}
