/* Christine Nguyen, CS163, Program 1, 01/21/2023
   The purpose of this program is to keep track of a users
   days and plans of their trip by build, travrese, destroy a LLL.
   The LLL will keep track of the days and the events planned for
   those days chronological order. In this list.h file there is a 
   dayLLL class that will implement a LLL of days where each day node
   will have infomation about a day, event, a next pointer, and the 
   inmoation for days and events are mostly dynamically allocated character
   arrays for day, time, activity, location, and one double for budget. The 
   functions in this class has are adding a day, adding a activity,
   deleting day/activity, finding an activity, and displaying. 
*/

#include <iostream>
#include <cctype>
#include <cstring>
#include <iomanip>

const int SIZE {50};	// constant size used for char arrays


// data needed for event node
struct event
{
        char * time;		// dynamically allocated char time of the event
        char * activity;	// dynamically allocated char activity name of the event
        char * location;	// dynamically allocated char location of the event
        double budget;		// the budget of the event
};


// node for event which has event data and pointer to next node
struct eventNode
{
        event a_event;		// data of the event node 
        eventNode * next;	// next pointer to next node 
};


// data needed for event node and a pointer to head of LLL of event nodes
struct Day
{
        char * day;
        eventNode * head;	// has an head pointer to eventNode 
};


// node for event which has day data and pointer to to next node
struct dayNode
{
        Day a_day;		// data of the day node: day and eventNode head
        dayNode * next;		// next pointer to next node
};


// this class is is to implement a day linked list with functions to add, delete, find, display
class dayLLL	//to use this class create an object in main (i.e. object.function() )
{
        public:
                dayLLL();       // default constructor, initialize data members to null
                ~dayLLL();      // deallocate any dynamic memory
                int addDay(char * userDay);	// adds a new day to day LLL with a specified input from user
						// return 1 on succuess and return 0 if day failed to enter
                int addActivity(char * userDay, char order, event & userEvent);	// add a new activity to a specified day, what order entered,
										// and event object return 1 on succuess and return 0 if failed to add
                int deleteDay(char * userDay);	// delete the day that matches the user's day as well as its activities 
						// return 1 on succuess and return 0 if failed
                int findActivity(char * userActivity);	// find days which has the activity the user specified in userActivity
							// return 1 on succuess and return 0 if failed to find
		int displayDay(char * userDay);	// display all the activities of the day the user specifies in userDay
						// return 1 on succuess and return 0 if failed to delete
                int displayAll();	// displays all the days and activities in the day LLL
					// return 1 on succuess and return 0 if LLL is empty

        private:
                dayNode * head;	// the head of the LLL
                dayNode * tail;	// the end of the LLL
		int deleteActivity(eventNode * & head); // is nested in deleteDay function, user does not have access to this function
};

