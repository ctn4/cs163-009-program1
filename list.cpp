#include "list.h"
using namespace std;

// The purpose of list.cpp is to implement the functions in the dayLLL class. 
// Creating a data structure, which the user could call in main to execute these
// functions to help with the planning of their day’s activities. The dayLLL() 
// and ~dayLLL() serve as the constructor and destructors. All functions are 
// int return type so if return 0 the user is able to output an error message in main.
// One private member deleteActivity will be called in deleteDay to delete all the 
// events associated dwith that day.


// constructor
dayLLL::dayLLL()
{
	 head = tail = NULL;
}


// destructor
dayLLL::~dayLLL()
{
    dayNode *temp = NULL;
    while (head)
    {
        // delete dynamic memory for char * members of each eventNode
        eventNode *curr = head->a_day.head;
        while (curr)
        {
            delete[] curr->a_event.time;
            delete[] curr->a_event.activity;
            delete[] curr->a_event.location;
            eventNode *tempEvent = curr->next;
            delete curr;
            curr = tempEvent;
        }

        // delete dynamic memory for char * members of Day
        delete[] head->a_day.day;

        // delete dayNode itself
        temp = head->next;
        delete head;
        head = temp;
    }
    tail = NULL;

}


// add day function adds a node in LLL by comparing if day is already added
int dayLLL::addDay(char * userDay)
{
	dayNode * current = head;

    // Check if the day already exists
    while (current != NULL)
    {
        if (strcasecmp(current->a_day.day, userDay) == 0)
        {
            return 0;
        }
        current = current->next;
    }

    // Create a new day node
    dayNode * newDay = new dayNode;
    newDay->a_day.day = new char[strlen(userDay) + 1];
    strcpy(newDay->a_day.day, userDay);
    newDay->a_day.head = NULL;	// set the event head node to null
    newDay->next = NULL;

    // Add the new day node to the linked list
    if (head == NULL) // List is empty
    {
        head = newDay;
        tail = newDay;
    }
    else
    {
        tail->next = newDay;
        tail = newDay;
    }

    return 1;
}


// add an activity to a day node by transversing day and eventNode head
int dayLLL::addActivity(char * userDay, char order, event & userEvent)
{
	dayNode * current = head; // go to the head and then transverse until null or we find a match
	while(current != NULL && strcasecmp(current->a_day.day, userDay) != 0)
	{
		current = current->next;
	}

	// return 0 there was no day found, if day was found the strcpy event into node
	if(current != NULL && strcasecmp(current->a_day.day, userDay) == 0)
	{
	eventNode * newEvent = new eventNode;
	newEvent->a_event.time = new char[strlen(userEvent.time) + 1];
	strcpy(newEvent->a_event.time, userEvent.time);
	newEvent->a_event.activity = new char[strlen(userEvent.activity) + 1];
	strcpy(newEvent->a_event.activity, userEvent.activity);
	newEvent->a_event.location = new char[strlen(userEvent.location) + 1];
	strcpy(newEvent->a_event.location, userEvent.location);
	newEvent->a_event.budget = userEvent.budget;
	newEvent->next = NULL;
	
	if(toupper(order) == 'Y')	// add to the front of the list
	{
		newEvent->next = current->a_day.head;
		current->a_day.head = newEvent;
	}	
	else
	{
		if(current->a_day.head == NULL)
			current->a_day.head = newEvent;
		else
		{
		eventNode * temp = current->a_day.head;
                while (temp->next != NULL)
                {
                    temp = temp->next;
                }
                temp->next = newEvent;
		}
        }
	return 1;
	}
	return 0;
}


// deletes the day calls the deleteActivity function to delete all the activities of that day
int dayLLL::deleteDay(char * userDay)
{
	dayNode * current = head;
	dayNode * prev = NULL;

	while(current != NULL && strcasecmp(current->a_day.day, userDay) != 0) 	// transverese to find matching day
	{
		prev = current;
		current = current->next;
	}

	if(current == NULL)
		return 0;
	deleteActivity(current->a_day.head);	// call delete activity function to delete the activities in that day
	if(current == head)
		head = current->next;
	else
		prev->next = current->next;
	if(current->a_day.day != NULL)
		delete [] current->a_day.day;		// deallocate memory
	delete current;
	return 1;
}


// deallocates the activities in an event LLL
int dayLLL::deleteActivity(eventNode * & head) //recursive, this function i can keep head in since the user does not 
					       //see this function, only used in delete day
{
	if(!head) //base case
		return 0;
	eventNode * current = head;
	head = head->next;

	delete [] current->a_event.time;	//deallocate memory
	delete [] current->a_event.activity;
	delete [] current->a_event.location;
	delete current;

	return 1 + deleteActivity(head);
}


// displays the days which have the activity the user wants to find by finding match with userActivity
int dayLLL::findActivity(char * userActivity)
{
	dayNode * dayCurrent = head;
	int match {0};	//0 no match 1 there is match
	while(dayCurrent != NULL)
	{
		eventNode * eventCurrent = dayCurrent->a_day.head;
		while(eventCurrent != NULL)
		{
			if(strcasecmp(eventCurrent->a_event.activity, userActivity) == 0)
			{
				match = 1;
				cout << endl << dayCurrent->a_day.day << endl;
			}
			eventCurrent = eventCurrent->next;
		}
		dayCurrent = dayCurrent->next;
	}
	if(match == 0)
		return 0;
	return 1;
}


// displays all the days and events in the LLL
int dayLLL::displayAll()
{
	if(!head)	//check if list is empty
		return 0;
	dayNode * dayCurrent = head;
	while(dayCurrent != NULL)
	{
		cout << "\n_________________" << endl;
		cout << "Day: " << dayCurrent->a_day.day << endl;

		eventNode * eventCurrent = dayCurrent->a_day.head;
		while(eventCurrent != NULL)
		{
			cout << "\nTime: " << eventCurrent->a_event.time << endl
			     << "Activity: " << eventCurrent->a_event.activity << endl
			     << "Location: " << eventCurrent->a_event.location << endl
			     << fixed << setprecision(2) << "Budget: $" << eventCurrent->a_event.budget << endl;
			eventCurrent = eventCurrent->next;
		}
		
		dayCurrent = dayCurrent->next;
	}
	return 1;
	
}


// displays a specific day the user pass through the argument userDay
int dayLLL::displayDay(char * userDay)
{
        dayNode * current = head;
	while(current != NULL && strcasecmp(current->a_day.day, userDay) != 0)
        {
                current = current->next;
        }
	if(current != NULL && strcasecmp(current->a_day.day, userDay) == 0)
	{
		cout << "\nActivities for "<< userDay << endl;

		eventNode * eventCurrent = current->a_day.head;
		while(eventCurrent != NULL)
		{
			cout << "_________________" << endl;
			cout << "\nTime: " << eventCurrent->a_event.time << endl
                             << "Activity: " << eventCurrent->a_event.activity << endl
                             << "Location: " << eventCurrent->a_event.location << endl
                             << fixed << setprecision(2) << "Budget: $" << eventCurrent->a_event.budget << endl;
                        eventCurrent = eventCurrent->next;
                }
		return 1;
	}
	return 0;
}

